let tweets = [];
let tweetId = 0;
var clickedTweet = null;
let support = false;

let sources = [
    {
        "name": "CNN",
        "leaning": "Left",
        "image": "/img/cnn.png"
    },
    {
        "name": "FoxNews",
        "leaning": "Right",
        "image": "/img/fox.png"
    }
];

let pfps = [
    {
        "name": "@johnj",
        "pfp": "/img/johnj.jpeg"
    },
    {
        "name": "@eliza123",
        "pfp": "/img/eliza123.jpeg"
    },
    {
        "name": "@jasonZ90",
        "pfp": "/img/jasonZ90.jpeg"
    }
];

function toggle() {
    support = !support;
    
    console.log(support);
    thisTweet = document.getElementById("supportForm");
    clickedTweet = tweets[thisTweet.parentElement.id];
}

function timeSince(time) {
  date = reviveDate(time);
  var seconds = Math.abs(new Date() - date) / 1000;

  var interval = seconds / 31536000;

  if (interval > 1) {
    return Math.floor(interval) + " years";
  }
  interval = seconds / 2592000;
  if (interval > 1) {
    return Math.floor(interval) + " months";
  }
  interval = seconds / 86400;
  if (interval > 1) {
    return Math.floor(interval) + " days";
  }
  interval = seconds / 3600;
  if (interval > 1) {
    return Math.floor(interval) + " hours";
  }
  interval = seconds / 60;
  if (interval > 1) {
    return Math.floor(interval) + " minutes";
  }
  return Math.floor(seconds) + " seconds";
}

function getFormattedDate(date) {
  if(date == null){
    return "";
  }
  var am_or_pm = "";
  var hour = date.getHours();
  if (hour > 12) {
    hour = hour % 12;
    am_or_pm = "PM";
  }
  else {
    am_or_pm = "AM";
  }

  var minute = date.getMinutes();

  var year = date.getFullYear();

  var month = (1+date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;

  var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;

  return hour + ":" + minute + " " + am_or_pm + " · " + months[month - 1] + ' ' + day + ', ' + year;
}

let reviveDate = function(strDate) {
  if(strDate == null)
    return null;

  let dateObj = new Date(strDate);
  if (isNaN(dateObj.getTime()))
  {
    dateObj = null;
  }

  return dateObj;
}

let renderHTML = function(index) {
    let tweet = tweets[index];
    let name = tweet.name;
    let atName = tweet.atName;
    let numComments = tweet.comments;
    let numLikes = parseInt(tweet.likes, 10);
    let numRetweets = parseInt(tweet.retweets, 10);
    let text = tweet.text;
    let id = tweet.numId;
    let date = tweet.createdDate;
    let retweetId = tweet.retweetId;
    
    var pfp = "";
    
    for (i = 0; i < pfps.length; i++) {
        if (atName == pfps[i].name) {
            pfp = pfps[i].pfp;
        }
    }
    
    var tr = "<tr id=";
    
    tr += "\"" + tweetId + "\" class=\"\"><td class=\"profile-tweet\"><span class=\"circle\" style=\"width:49px; height:49px; margin:10px; background-image: url('" + pfp + "'); background-repeat: no-repeat; background-size: 50px 50px;\"></span><div class=\"header-wrap\"><h5 class=\"tweet-name\">" + name + "</h5><h6 class=\"tweet-at\" style=\"float: right; font-weight:800\">...</h6><h6 class=\"tweet-at\">" + atName + "</h6></div><div class=\"tweet-text\"><p>" + text + "</p></div>";
    
    if (retweetId > -1) {
        for (i = 0; i < (tweets.length); i++) {
            if (retweetId == tweets[i].numId) {
                let rTwee = tweets[i];
                var retwpfp = "";
                for (k = 0; k < pfps.length; k++) {
                    if (rTwee.atName == pfps[k].name) {
                        retwpfp = pfps[k].pfp;
                    }
                }
                
                tr += "<div class=\"retweet\"><span class=\"circle\" style=\"width:30px; height:30px; margin:10px; background-image: url('" + retwpfp + "'); background-repeat: no-repeat; background-size: 30px 30px;\"></span><div class=\"header-wrap\"><a href=\"/tweet_details.html?id=" + rTwee.numId + "\"><h5 class=\"tweet-name\">" + rTwee.name + "</h5></a><h6 class=\"tweet-at\">" + rTwee.atName + "</h6><h6 class=\"tweet-at\"> · </h6><h6 class=\"tweet-at\">" + timeSince(rTwee.createdDate) + "</h6></div><div class=\"tweet-text\"><p>" + rTwee.text + "</p></div></div>";
                
                break;
            }
        }
    }
    
    tr += "<div class=\"time-date\"><p>" + getFormattedDate(reviveDate(date)) + "</p></div>";
    
    if (numRetweets > 0 || numLikes > 0) {
        tr += "<div class=\"tweet-nums\">";
        if (numRetweets > 0) {
            tr += "<h4>" + numRetweets + "</h4> <p style=\"margin-right: 20px;\">Retweets</p>";
        }

        if (numLikes > 0) {
            tr += "<h4>" + numLikes + "</h4> <p>Likes</p>";
        }
        tr += "</div>"
    }
    
    tr += "<div class=\"tweet-footer\"><img src=\"img/comments.svg\" class=\"btTxt comment\" id=\"commentForm\"><input type=\"image\" data-toggle=\"modal\" data-target=\"#retwModal\" src=\"img/retweet.svg\" class=\"retw\" id=\"retweetForm\" style=\"width:30px; height:30px; margin-bottom: -5;\"/>";
    
    if (tweet.support > -1) {
        tr += "<input type=\"image\" src=\"img/handshake.svg\" name=\"likeForm\" class=\"btTxt like\" id=\"likeForm\" style=\"width:30px; height:30px;\" />";
    } else {
        tr += "<input type=\"image\" src=\"img/heart.svg\" name=\"likeForm\" class=\"btTxt like\" id=\"likeForm\" />";
    }
    
    tr += "<img src=\"img/share.svg\" style=\"margin-right:0;\"></div></td></tr>";
    
    return tr;
}

let renderSource = function(index) {
    let tweet = tweets[index];
    let sourceName = tweet.source.split(".")[0].toLowerCase();
    var source = null;
    $("#source").empty();
    
    for (i = 0; i < sources.length; i++) {
        if (sourceName == sources[i].name.toLowerCase()) {
            source = sources[i];
        }
    }
    
    if (source != null) {
        let add = "<a href=\"http://" + tweet.source + "\" target=\"_blank\" style=\"display: block\"><h3>Source:</h3><img src=\"" + source.image + "\"</img><div class=\"sourceInfo\"><h4>" + source.name + "</h4><h5>" + source.leaning + " leaning</h5></div></a>";
        
        return add;
    }
    return "";
    
}

let renderTweet = function() {
    console.log("rendering");
    $("tbody").empty();
    
    for (k = 0; k < tweets.length; k++) {
        if (tweets[k].numId == tweetId) {
            $("#source").append(renderSource(k));
            $("#tweet>tbody").append(renderHTML(k));
            break;
        }
    }
}

let fetchData = function() {
    let params = new URLSearchParams(location.search);
    tweetId = params.get("id");
    
    
    $.get("fetchtweets", null, function(data, status, obj) {
        tweets = [];
        for (let i = 0; i < data.data.length; i++) {
          tweets.push(data.data[i]);
          tweets[i].completedDate = reviveDate(tweets[i].completedDate);
        }
        renderTweet();
    });
}

$(document).ready(function() {
    fetchData();
    
    $("#submitTweet").click(function( event ) {
        numTweets++;
        event.preventDefault();
        let name = "John Jefferies";
        let atName = "@johnj";
        let text = $("#tweetText").val();
        let source = $("#tweetSource").val();
        
        let newTweet = {
            name: name,
            atName: atName,
            text: text,
            comments: 0,
            retweets: 0,
            likes: 0,
            retweetId: 0,
            support: 0,
            numId: numTweets + 1,
            createdDate: new Date(),
            source: source
        };
        
        $.post("newtweet", newTweet)
        .done(function(data, status, obj) {
            fetchData();
        });
        $("#tweetText").val("");
        $("#tweetSource").val("");
    });
    
    $("#retweetTweet").click(function( event ) {
        event.preventDefault();
        let name = "John Jefferies";
        let atName = "@johnj";
        let text = $("#retweetText").val();
        let supportId = -1;
        if (support) {
            supportId = clickedTweet.numId;
        }
        
        let newTweet = {
            name: name,
            atName: atName,
            text: text,
            comments: 0,
            retweets: 0,
            likes: 0,
            retweetId: clickedTweet.numId,
            support: supportId,
            numId: (tweets.length),
            source: ""
        };
        
        $.post("newtweet", newTweet)
        .done(function(data, status, obj) {
        });
        var numRetweets = clickedTweet.retweets + 1;
        $.post("updateretweets", {_id: clickedTweet._id, data: {retweets: numRetweets}})
        .done(function(data, status, obj) {
            fetchData();
        });
        $("#retweetText").val("");
    });
});

$(document).on('click', '.like', function() {
    console.log('Clicked like');
    clickedTweet = tweets[this.parentElement.parentElement.parentElement.id];
    var numLikes = clickedTweet.likes + 1;
    var newTweet = null;
    
    if (clickedTweet.support > -1) {
        for (i = 0; i < (tweets.length); i++) {
            if (tweets[i].numId == clickedTweet.support) {
                newTweet = tweets[i];
            }
        }
    }
    
    if (newTweet != null) {
        var oldLikes = newTweet.likes;
        $.post("updatetweet", {_id: newTweet._id, data: {likes: oldLikes + 1}})
        .done(function(data, status, obj) {
            fetchData();
        });
    }
    $.post("updatetweet", {_id: clickedTweet._id, data: {likes: numLikes}})
    .done(function(data, status, obj) {
        fetchData();
    });
});

$(document).on('click', '.retw', function() {
    console.log('Clicked retweet');
    clickedTweet = tweets[this.parentElement.parentElement.parentElement.id];
    var retwName = clickedTweet.name;
    var retwAtName = clickedTweet.atName;
    var retwText = clickedTweet.text;
    var retwDate = clickedTweet.createdDate;
   
    document.getElementById("retwName").textContent=retwName;
    document.getElementById("retwAtName").textContent=retwAtName;
    document.getElementById("retwText").textContent=retwText;
    document.getElementById("retwId").setAttribute("id", clickedTweet.numId);
    
    var newStyle = "width: 20px; height: 20px; background-image: url(\'/img/" + retwAtName.split("@")[1] + ".jpeg')\; background-repeat: no-repeat; background-size: 20px 20px; float: left; margin: 10px;";
    document.getElementById("retwPfp").setAttribute("style", newStyle);
});