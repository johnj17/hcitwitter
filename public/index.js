let tweets = [];
let numTweets = 0;
var clickedTweet = null;
let support = false;

let pfps = [
    {
        "name": "@johnj",
        "pfp": "/img/johnj.jpeg"
    },
    {
        "name": "@eliza123",
        "pfp": "/img/eliza123.jpeg"
    },
    {
        "name": "@jasonZ90",
        "pfp": "/img/jasonZ90.jpeg"
    }
];

function toggle() {
    support = !support;
    
    console.log(support);
    thisTweet = document.getElementById("supportForm");
    clickedTweet = tweets[thisTweet.parentElement.id];
}

function timeSince(time) {
  timeStamp = reviveDate(time);
  var now = new Date();
  secondsPast = Math.abs(now - timeStamp) / 1000;
  if (secondsPast < 60) {
    return parseInt(secondsPast) + 's';
  }
  if (secondsPast < 3600) {
    return parseInt(secondsPast / 60) + 'm';
  }
  if (secondsPast <= 86400) {
    return parseInt(secondsPast / 3600) + 'h';
  }
  if (secondsPast > 86400) {
    day = timeStamp.getDate();
    month = timeStamp.toDateString().match(/ [a-zA-Z]*/)[0].replace(" ", "");
    year = timeStamp.getFullYear() == now.getFullYear() ? "" : " " + timeStamp.getFullYear();
    return day + " " + month + year;
  }
}

let reviveDate = function(strDate) {
  if(strDate == null)
    return null;

  let dateObj = new Date(strDate);
  if (isNaN(dateObj.getTime()))
  {
    dateObj = null;
  }

  return dateObj;
}


let renderHTML = function(index) {
    let name = index.name;
    let atName = index.atName;
    let numComments = index.comments;
    let numLikes = index.likes;
    let numRetweets = index.retweets;
    let text = index.text;
    let id = index.numId;
    let date = index.createdDate;
    let retweetId = index.retweetId;
    
    var pfp = "";
    
    for (i = 0; i < pfps.length; i++) {
        if (atName == pfps[i].name) {
            pfp = pfps[i].pfp;
        }
    }
    
    var tr = "<tr id=";
    
    tr += "\"" + id + "\" class=\"\"><td class=\"profile-tweet\"><span class=\"circle\" style=\"width:49px; height:49px; margin:10px; background-image: url('" + pfp + "'); background-repeat: no-repeat; background-size: 50px 50px;\"></span></td><td class=\"\"><div class=\"header-wrap\"><a href=\"/tweet_details.html?id=" + id + "\"><h5 class=\"tweet-name\">" + name + "</h5></a><h6 class=\"tweet-at\">" + atName + "</h6><h6 class=\"tweet-at\"> · </h6><h6 class=\"tweet-at\">" + timeSince(date) + "</h6><h6 class=\"tweet-at\" style=\"float: right; font-weight:800\">...</h6></div><div class=\"tweet-text\"><p>" + text + "</p>";
    
    if (retweetId > -1) {
        for (i = 0; i < (tweets.length); i++) {
            if (retweetId == tweets[i].numId) {
                let rTwee = tweets[i];
                var retwpfp = "";
                for (k = 0; k < pfps.length; k++) {
                    if (rTwee.atName == pfps[k].name) {
                        retwpfp = pfps[k].pfp;
                    }
                }
                tr += "<div class=\"retweet\"><span class=\"circle\" style=\"width:30px; height:30px; margin:10px; background-image:url('" + retwpfp + "'); background-repeat: no-repeat; background-size: 30px 30px; float:left;\"></span><div class=\"header-wrap\"><a href=\"/tweet_details.html?id=" + rTwee.numId + "\"><h5 class=\"tweet-name\">" + rTwee.name + "</h5></a><h6 class=\"tweet-at\">" + rTwee.atName + "</h6><h6 class=\"tweet-at\"> · </h6><h6 class=\"tweet-at\">" + timeSince(rTwee.createdDate) + "</h6></div><div class=\"tweet-text\"><p>" + rTwee.text + "</p></div></div>"
            }
        }
    }
    
    tr += "</div><div class=\"tweet-footer\"><input type=\"image\" src=\"img/comments.svg\" name=\"commentForm\" class=\"btTxt comment\" id=\"commentForm\" /><p>" + numComments + "</p><input type=\"image\" data-toggle=\"modal\" data-target=\"#retwModal\" src=\"img/retweet.svg\" class=\"retw\" id=\"retweetForm\" style=\"width:30px; height:30px; margin-bottom: -5;\"/><p>" + numRetweets + "</p>";
    
    if (index.support > -1) {
        tr += "<input type=\"image\" src=\"img/handshake.svg\" name=\"likeForm\" class=\"btTxt like\" id=\"likeForm\" style=\"width:30px; height:30px;\" />";
    } else {
        tr += "<input type=\"image\" src=\"img/heart.svg\" name=\"likeForm\" class=\"btTxt like\" id=\"likeForm\" />";
    }
    
    tr += "<p>" + numLikes + "</p><img src=\"img/share.svg\"></div></td></tr>";
    
    numTweets++;
    return tr;
}

let renderTweets = function() {
    console.log("rendering");
    $("tbody").empty();
    numTweets = 0;
    
    for ( let i = (tweets.length) - 1; i >= 0; i--) {
        $("#tweets>tbody").append(renderHTML(tweets[i]));
    }
}

let fetchData = function() {
    $.get("fetchtweets", null, function(data, status, obj) {
        tweets = data.data;
        renderTweets();
    });
}

$(document).ready(function() {
    fetchData();
    
    $("#submitTweet").click(function( event ) {
        numTweets++;
        event.preventDefault();
        let name = "John Jefferies";
        let atName = "@johnj17";
        let text = $("#tweetText").val();
        let source = $("#tweetSource").val();
        
        let newTweet = {
            name: name,
            atName: atName,
            text: text,
            comments: 0,
            retweets: 0,
            likes: 0,
            retweetId: -1,
            support: -1,
            numId: numTweets - 1,
            source: source
        };
        
        $.post("newtweet", newTweet)
        .done(function(data, status, obj) {
            fetchData();
        });
        $("#tweetText").val("");
        $("#tweetSource").val("");
    });
    
    $("#retweetTweet").click(function( event ) {
        numTweets++;
        event.preventDefault();
        let name = "John Jefferies";
        let atName = "@johnj17";
        let text = $("#retweetText").val();
        let supportId = -1;
        if (support) {
            supportId = clickedTweet.numId;
        }
        
        let newTweet = {
            name: name,
            atName: atName,
            text: text,
            comments: 0,
            retweets: 0,
            likes: 0,
            retweetId: clickedTweet.numId,
            support: supportId,
            numId: numTweets - 1,
            source: ""
        };
        
        $.post("newtweet", newTweet)
        .done(function(data, status, obj) {
        });
        var numRetweets = clickedTweet.retweets + 1;
        $.post("updateretweets", {_id: clickedTweet._id, data: {retweets: numRetweets}})
        .done(function(data, status, obj) {
            fetchData();
        });
        $("#retweetText").val("");
    });
    
});

$(document).on('click', '.like', function() {
    console.log('Clicked like');
    clickedTweet = tweets[this.parentElement.parentElement.parentElement.id];
    var numLikes = clickedTweet.likes + 1;
    var newTweet = null;
    
    if (clickedTweet.support > -1) {
        for (i = 0; i < (tweets.length); i++) {
            if (tweets[i].numId == clickedTweet.support) {
                newTweet = tweets[i];
            }
        }
    }
    
    if (newTweet != null) {
        var oldLikes = newTweet.likes;
        $.post("updatetweet", {_id: newTweet._id, data: {likes: oldLikes + 1}})
        .done(function(data, status, obj) {
            fetchData();
        });
    }
    $.post("updatetweet", {_id: clickedTweet._id, data: {likes: numLikes}})
    .done(function(data, status, obj) {
        fetchData();
    });
});

$(document).on('click', '.retw', function() {
    console.log('Clicked retweet');
    clickedTweet = tweets[this.parentElement.parentElement.parentElement.id];
    var retwName = clickedTweet.name;
    var retwAtName = clickedTweet.atName;
    var retwText = clickedTweet.text;
    var retwDate = clickedTweet.createdDate;
   
    document.getElementById("retwName").textContent=retwName;
    document.getElementById("retwAtName").textContent=retwAtName;
    document.getElementById("retwText").textContent=retwText;
    document.getElementById("retwId").setAttribute("id", clickedTweet.numId);
    
    var newStyle = "width: 20px; height: 20px; background-image: url(\'/img/" + retwAtName.split("@")[1] + ".jpeg')\; background-repeat: no-repeat; background-size: 20px 20px; float: left; margin: 10px;";
    document.getElementById("retwPfp").setAttribute("style", newStyle);
});