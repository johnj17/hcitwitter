
const {ObjectId} = require('mongodb');
let express = require("express");
var bodyParser   = require('body-parser');
let app = express();
let dbo = null;
let VERBOSE = true;

app.use(express.urlencoded({ extended: true }))
app.use(express.json());
app.use(express.static('public'));

//make way for some custom css, js and images
app.use('/css', express.static(__dirname + '/public/css'));
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/images', express.static(__dirname + '/public/img'));


let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/TwitterHCI";

MongoClient.connect(url, { useUnifiedTopology: true }, function(err, db) {
  if (err) throw err;

  dbo = db.db("TwitterHCIM");
  if(VERBOSE)console.log("Database created!");

  dbo.createCollection("tweets", function(err, res) {
    if (err) throw err;
    if(VERBOSE)console.log("Collection tweets created!");
  });
});

let insertDocument = function(db, collectionName, data, callback) {
  db.collection(collectionName).insertOne( data, function(err, result) {
    if(VERBOSE)console.log("insertDocument: Inserted a document into the "+collectionName+" collection. : " + data._id);
    if(callback)callback(data);
  });
};

let updateOneDocument = function(db, collectionName, query, newvalues, callback) {
  if(VERBOSE)console.log("updateOneDocument: query:" + JSON.stringify(query));
  if(VERBOSE)console.log("updateOneDocument: newValue:" + JSON.stringify(newvalues));
  db.collection(collectionName).updateOne(query,{ $set: newvalues }, function(err, res) {
    if (err) {
      throw err;
      if(callback)callback(err);
    }
    if(VERBOSE)console.log("updateOneDocument: Updated a document in "+collectionName+" collection. ");

    if(callback)callback();
  });
};

//https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
let writeOKResponse = function(res, message, data){
  let obj = {
    status: "OK",
    message: message,
    data: data
  };
  if(VERBOSE)console.log("writeOKResponse:" + message);

  res.writeHead(200, {'Content-type': 'application/json'});
  res.end(JSON.stringify(obj));
}

let writeBadRequestResponse = function(res, message){
  if(VERBOSE)console.log("writeBadRequestResponse:" + message);
  res.writeHead(400, {'Content-type': 'text/plain'});
  res.end(message);
}

let retreiveAllDocsList = function(db, query, fields,sort ){
  try {
    cursor = db.collection('tweets').find(query).sort(sort).project(fields);
  } catch (err) {
    if(VERBOSE)console.log("retreiveAllDocsList: retreiveAllDocsList Error\n" + err);
  }
  return cursor;
};

app.get('/fetchtweets', function (req, res) {
  if(VERBOSE)console.log("/fetchtweets request");
  let list = retreiveAllDocsList(
    dbo
    , {deleted:false}
    , { _id: 1, name: 1 , atName: 1, text: 1, comments:1, likes: 1, retweets:1, numId: 1, createdDate: 1, retweetId: 1, support: 1, source: 1}
    , {createdDate: 1}
  );
  list.toArray(function(err, docs){
    if(err){
      writeBadRequestResponse(res, "fetchtweets: Error in fetching data: " + err);
    }
    else{
      writeOKResponse(res, "fetchtweets: Succesfully Fetched Tweets Data ", docs);
    }
  });
});

app.get('/findonetweet', function (req, res) {
    if(VERBOSE)console.log("/fetchonetweet request");
    let tweetId = req.body.numId;
    query = {numId: tweetId};
    
    try {
        cursor = dbo.collection('tweets').findOne(query);
    } catch (err) {
        if(VERBOSE)console.log("findOne: findOne Error\n" + err);
    }
    return cursor;
})

app.post('/updatetweet', function (req, res) {
  if(VERBOSE)console.log("/updatetweet");

  let tweet_id  = req.body._id; // provide the _id
  let tweet_data  = req.body.data;

  if (tweet_id == undefined){
    writeBadRequestResponse(res, "updatetweet: _id not defined.");
    return;
  }

  if (tweet_data == undefined){
    writeBadRequestResponse(res, "updatetweet: data for id("+tweet_id+") not defined.");
    return;
  }

  for (let j=0; j< Object.keys(tweet_data).length; j++){
    if (!["likes"].includes(Object.keys(tweet_data)[j])){
      writeBadRequestResponse(res, "updatetweet: unknown update field("+Object.keys(tweet_data)[j]+").");
      return;
    }
  }
    
    var numLikes = parseInt(tweet_data.likes, 10);

  if (typeof(numLikes) != "number"){
    writeBadRequestResponse(res, "updatetweet: likes needs to be number("+numLikes+").");
    return;
  }
    
    tweet_data.likes = numLikes;

  updateOneDocument(dbo, "tweets",   {_id:ObjectId(tweet_id)}, tweet_data, function(err){
    if(err){
        writeBadRequestResponse(res, "updatetweet: Update Document Failed" + err);
        return;
    }
    writeOKResponse(res, "updatetweet: Updated Successfully("+tweet_id+")", {_id: tweet_id});
  });
});

app.post('/updateretweets', function (req, res) {
  if(VERBOSE)console.log("/updatetweet");

  let tweet_id  = req.body._id; // provide the _id
  let tweet_data  = req.body.data;

  if (tweet_id == undefined){
    writeBadRequestResponse(res, "updatetweet: _id not defined.");
    return;
  }

  if (tweet_data == undefined){
    writeBadRequestResponse(res, "updatetweet: data for id("+tweet_id+") not defined.");
    return;
  }

  for (let j=0; j< Object.keys(tweet_data).length; j++){
    if (!["retweets"].includes(Object.keys(tweet_data)[j])){
      writeBadRequestResponse(res, "updatetweet: unknown update field("+Object.keys(tweet_data)[j]+").");
      return;
    }
  }
    
    var numRetweets = parseInt(tweet_data.retweets, 10);

  if (typeof(numRetweets) != "number"){
    writeBadRequestResponse(res, "updateretweets: retweets needs to be number("+numRetweets+").");
    return;
  }
    
    tweet_data.retweets = numRetweets;

  updateOneDocument(dbo, "tweets",   {_id:ObjectId(tweet_id)}, tweet_data, function(err){
    if(err){
        writeBadRequestResponse(res, "updateretweets: Update Document Failed" + err);
        return;
    }
    writeOKResponse(res, "updateretweets: Updated Successfully("+tweet_id+")", {_id: tweet_id});
  });
});

app.post('/newtweet', function (req, res) {
  let tweet = req.body;

  if(typeof(tweet.name)!="string"){
    writeBadRequestResponse(res, "newtweet: No name is defined.");
    return;
  }

  if(typeof(tweet.text)!="string"){
    writeBadRequestResponse(res, "newtask: No due date is defined.");
    return;
  }

  // default data.
  tweet.createdDate= new Date(),
  tweet.deleted = false;

  insertDocument(dbo, "tweets", tweet, function(data){
    writeOKResponse(res, "newtask: Created Successfully", {_id: data._id});
  });
});

let server = app.listen(8081, function(){
    let port = server.address().port;
    if(VERBOSE)console.log("Server started at http://localhost:%s", port);
});
